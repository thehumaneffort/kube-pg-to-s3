FROM alpine:3.4

RUN \
	mkdir -p /aws && \
	apk -Uuv add groff less python py-pip && \
	pip install awscli && \
	apk --purge -v del py-pip && \
	rm /var/cache/apk/*

ADD https://storage.googleapis.com/kubernetes-release/release/v1.5.1/bin/linux/amd64/kubectl /usr/local/bin/kubectl

ENV HOME=/config

RUN set -x && \
    apk add --no-cache curl ca-certificates && \
    chmod +x /usr/local/bin/kubectl && \
    \
    # Create non-root user (with a randomly chosen UID/GUI).
    adduser kubectl -Du 2342 -h /config && \
    \
    # Basic check it works.
    kubectl version --client

RUN apk add --update curl && \
    rm -rf /var/cache/apk/*

COPY backup /bin/backup
COPY notify_error /bin/notify_error

COPY crontab /var/spool/cron/crontabs/root

RUN chmod +x /bin/backup
RUN chmod +x /bin/notify_error

CMD crond -d 2 -f
